const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

async function SendSQSMerchantCallback(params) {
  console.log('SendSQSMerchantCallback',params)
  let param = {
    DelaySeconds: 3,
    MessageBody: params,
    QueueUrl: process.env.SQS_PAYMENT_MERCHANT_REF
  };
  await sqs.sendMessage(param).promise()
}

module.exports = { SendSQSMerchantCallback };





