const AWS = require('aws-sdk');
AWS.config.update({ region: `${process.env.REGION}` });
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });


async function getPaymentById(paymentId) {
  const params = {
    TableName : process.env.PAYMENTS_TABLE,
    Key: {
        payment_id:{
            S: paymentId
        }
    }
  }
  let { Item } = await dynamodb.getItem(params).promise()
  return Item
  
}

module.exports = { getPaymentById };
