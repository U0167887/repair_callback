require('dotenv').config();
const paymentList = require('./paymentList')
const { getPaymentById } = require('./service/getPaymentById');
const { SendSQSMerchantCallback } = require('./service/SendsqsMerchantCallback');
const path = require('path');
const fs = require('fs');
let outputListOK = []
let outputListError = []

async function main(){
    console.log('START')
    for(let i=0; i<paymentList.length; i++){
        let paramsSQS = {}
        try {
            //get Payment in dynamo
            const payment = await getPaymentById(paymentList[i])
            console.log('payment',payment)
            //valid status and data required
            if(payment.payment_id && (payment.status.S == 'APPROVED' || payment.status.S == 'REJECTED')){
                //mapping params
                paramsSQS = {
                    payment_callback_url: 'https://ycer-experience.bff.bancogalicia.com.ar/api/v1/payment/callback',
                    payment_id: paymentList[i],
                    payment_check_url: `https://checkout.apinaranja.com/api/payments/${paymentList[i]}`
                }
                //parse data for SQS
                paramsSQS = JSON.stringify(paramsSQS)
                //send to sqs merchantCallback
                console.log('paramsSQS',paramsSQS)
                // await SendSQSMerchantCallback(paramsSQS)
                outputListOK.push(payment.payment_id.S)
                await delay(5000)
            }else{
                outputListError.push(payment.payment_id.S)
            }
            
        } catch (error) {
            console.log(error)
        }
    }
    fs.writeFileSync(
        path.resolve(__dirname, './output/PROCESSED.JSON'),
        JSON.stringify(outputListOK)
    );
    fs.writeFileSync(
        path.resolve(__dirname, './output/NOT_PROCESSED.JSON'),
        JSON.stringify(outputListError)
    );
    console.log('outputListOK', outputListOK.length)
    console.log('outputListError', outputListError.length)
}


main()

function delay(n){
    return new Promise(function(resolve){
        setTimeout(resolve,n);
    });
}